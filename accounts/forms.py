from django import forms
from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class loginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignupForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]


class createExpense(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class createAccount(ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
