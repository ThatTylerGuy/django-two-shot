from django.urls import path
from receipts.views import Signin, user_logout, CreateUser

urlpatterns = [
    path("login/", Signin, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", CreateUser, name="signup"),
]
