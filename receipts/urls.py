from django.urls import path
from receipts.views import (
    receipt_instance,
    CreateReceipt,
    expense_category,
    account_list,
    create_expense,
    create_account,
)

urlpatterns = [
    path("", receipt_instance, name="home"),
    path("create/", CreateReceipt, name="create_receipt"),
    path("categories/", expense_category, name="category"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_expense, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
