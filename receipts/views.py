from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from accounts.forms import (
    loginForm as log,
    SignupForm,
    ReceiptForm,
    createExpense,
    createAccount,
)
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


# Create your views here.
@login_required
def receipt_instance(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_inst": receipt}
    return render(request, "receipts/receipts.html", context)


def redirect_to_receipts(request):
    return redirect(receipt_instance)


def Signin(request):
    if request.method == "POST":
        form = log(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = log()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def CreateUser(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignupForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)


@login_required
def CreateReceipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def expense_category(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": category}
    return render(request, "receipts/categories_list.html", context)


@login_required
def create_expense(request):
    if request.method == "POST":
        form = createExpense(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            form.save()
            return redirect("category")
    else:
        form = createExpense()
    context = {"form": form}
    return render(request, "receipts/createExpense.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = createAccount(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = createAccount()
    context = {"form": form}
    return render(request, "receipts/createAccount.html", context)


@login_required
def account_list(request):
    list = Account.objects.filter(owner=request.user)
    context = {"accounts": list}
    return render(request, "receipts/account.html", context)
